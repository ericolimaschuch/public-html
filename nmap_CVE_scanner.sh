#!/bin/bash
sudo git clone https://github.com/scipag/vulscan          /usr/share/nmap/scripts/vulscan
sudo git clone https://github.com/vulnersCom/nmap-vulners /usr/share/nmap/scripts/vulners
sudo nmap --script-updatedb
sudo nmap -O -sV -Pn  --script=vulners/vulners.nse ${1} -oA ${1}.vulvers &
sudo nmap -O -sV -Pn  --script=vulscan/vulscan.nse ${1} -oA ${1}.vulscan 
sudo chown ${USER}.${GROUP} ${1}.* 
xsltproc ${1}.vulvers.xml -o ${1}.vulvers.html
xsltproc ${1}.vulscan.xml -o ${1}.vulscan.html
xdg-open ${1}.vulscan.html &
xdg-open ${1}.vulvers.html &
